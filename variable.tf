variable "amiid" {
	default  = "ami-0c7217cdde317cfec"
}

variable "type" {
 	default ="t2.micro"
}

variable "user" {
	default ="ubuntu"
}

variable "pemfile" {
	default = "AWSkeypair"
}
